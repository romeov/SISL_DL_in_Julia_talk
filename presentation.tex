% Created 2023-10-09 Mon 00:36
% Intended LaTeX compiler: lualatex
\documentclass[presentation,10pt]{beamer}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{/home/romeo/Documents/latex_playground/julia-tufte-beamer/tex/beamerthemetufte}
\institute{Stanford University}
\email{romeov@stanford.edu}
\usetheme{default}
\author{Romeo Valentin}
\date{}
\title{State of Deep Learning in Julia}
\hypersetup{
 pdfauthor={Romeo Valentin},
 pdftitle={State of Deep Learning in Julia},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.1 (Org mode 9.7)}, 
 pdflang={English}}
\usepackage{biblatex}

\begin{document}

\maketitle
\section{Motivation}
\label{sec:org1a3aa9e}
\begin{frame}[label={sec:org91f7742}]{A brief history of ML frameworks}
\begin{center}
\includegraphics[width=\linewidth]{/home/romeo/Documents/Stanford/SISL/JuliaDLTalk/Timeline-of-deep-learning-frameworks.png}
\end{center}
\end{frame}
\begin{frame}[label={sec:org94187cc},t]{What does a modern ML framework need to provide?}
\phantom{Python}
\begin{columns}
\begin{column}[t]{0.5\columnwidth}
\setbeamercovered{transparent}
\begin{itemize}[<+->]
\item Array/tensor programming
\item Auto differentiation
\item Pre-built ``layers''
\item Pre-built optimizers
\item Execute on GPU/TPU
\end{itemize}
\end{column}
\begin{column}[t]{0.5\columnwidth}
\pause

\setbeamercovered{transparent}
\begin{itemize}[<+->]
\item IR optimization
\item Hardware-specific optim.
\item Data loading \& processing
\item Distributed computing
\item Inference in other contexts
\item Sharing \& reusing models
\item Experiment tracking
\item Experiment sweeps \& optimization
\end{itemize}
\end{column}
\end{columns}
\begin{block}{\phantom{Finally}}
\pause
\noindent
\begin{itemize}
\item Hackability -> change or implement functionality
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgc4ef42f},t]{What does a modern ML framework need to provide?}
Python \(\rightarrow\) external libraries do most of the heavy lifting.
\begin{columns}
\begin{column}[t]{0.5\columnwidth}
\begin{itemize}
\item Array/tensor programming
\myconstruction
\item Auto differentiation
\myconstruction
\item Pre-built ``layers''
\mycheck
\item Pre-built optimizers
\myconstruction
\item Execute on GPU/TPU
\myconstruction
\end{itemize}
\end{column}
\begin{column}[t]{0.5\columnwidth}
\begin{itemize}
\item IR optimization
\mycross
\item Hardware-specific optim.
\myconstruction
\item Data loading \& processing
\myconstruction
\item Distributed computing
\mycross
\item Inference in other contexts
\myconstruction
\item Sharing \& reusing models
\mycheck
\item Experiment tracking
\mycheck
\item Experiment sweeps \& optimization
\mycheck
\end{itemize}
\end{column}
\end{columns}
\begin{block}{\phantom{Finally}}
\noindent
\begin{itemize}
\item Hackability -> change or implement functionality
\mycross
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgc369308},t]{What does a modern ML framework need to provide?}
Julia can do many things natively.
\begin{columns}
\begin{column}[t]{0.5\columnwidth}
\begin{itemize}
\item Array/tensor programming
\mycheck
\item Auto differentiation
\mycheck
\item Pre-built ``layers''
\mycheck
\item Pre-built optimizers
\mycheck
\item Execute on GPU/TPU
\myconstruction
\end{itemize}
\end{column}
\begin{column}[t]{0.5\columnwidth}
\begin{itemize}
\item IR optimization
\mycheck
\item Hardware-specific optim.
\mycross
\item Data loading \& processing
\mycheck
\item Distributed computing
\mycheck
\item Inference in other contexts
\myconstruction
\item Sharing \& reusing models
\myconstruction
\item Experiment tracking
\mycheck
\item Experiment sweeps \& optimization
\mycheck
\end{itemize}
\end{column}
\end{columns}
\begin{block}{\phantom{Finally}}
\noindent
\begin{itemize}
\item Hackability -> change or implement functionality
\mycross
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org3583f9c}]{Why did Python ``win''?}
\begin{itemize}
\item First of all: \alert{Python/Pytorch is great!}
\item Python knowledge is ubiquitous
\item Very well developed ecosystem
\item Large amounts of models, datasets
\item \ldots{}however
\end{itemize}
\pause
\begin{itemize}
\item effectively DSL within python
\item everything performance critical is a (C++) library
\end{itemize}
\end{frame}
\section{Practical Matters}
\label{sec:orgff169f0}
\begin{frame}[label={sec:org5592fe4},fragile]{ML Domains}
 \begin{columns}
\begin{column}[t]{0.3\columnwidth}
Computer Vision \(\rightarrow\) \alert{\texttt{Metalhead.jl}}:
\pause
{\tiny
\begin{itemize}
\item ConvMixer
\item ConvNeXt
\item DenseNet
\item EfficientNetv1/v2
\item gMLP
\item GoogLeNet
\item Inception-v3/v4
\item MLPMixer
\item MobileNetv1/v2/v3
\item MNASNet
\item ResNet/ResNeXt
\item SqueezeNet
\item UNet
\item Vision Transformer
\end{itemize}
}
\end{column}
\begin{column}[t]{0.3\columnwidth}
\pause
NLP \(\rightarrow\) \alert{\texttt{Transformers.jl}}:
\pause
{\tiny
\begin{itemize}
\item bart
\item bert
\item bloom
\item clip
\item gpt
\item gpt2
\item gpt neo
\item gpt neox
\item gptj
\item llama
\item roberta
\item t5
\end{itemize}

}
\end{column}
\begin{column}[t]{0.3\columnwidth}
\pause
Others:
{\tiny
\begin{itemize}
\item PyCallChainRules.jl / PyTorchTraining.jl \(\rightarrow\) gradient across Python/Julia
\item Neural Differential Equations: DiffEqFlux.jl and NeuralPDE.jl
\item Geometric Deep Learning: GeometricFlux.jl
\item ObjectDetector.jl w/ YOLO etc
\end{itemize}
}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:orgedd21a3}]{Other ML Tooling}
\begin{center}
\begin{tabular}{lll}
\alert{Python} & \alert{Julia} & comments\\[0pt]
\hline
wandb & Wandb.jl & also tensorboard\\[0pt]
Pytorch Lightning & FluxTraining.jl & \\[0pt]
 & ParameterSchedulers.jl & \\[0pt]
Distributed Training & DaggerFlux.jl & (model parallel)\\[0pt]
 & FluxMPI.jl & (data parallel)\\[0pt]
Huggingface & HuggingFaceApi.jl & \\[0pt]
FastAI & FastAI.jl & \\[0pt]
CUDA/AMD/Apple & \small CUDA.jl/AMDGPU.jl/Metal.jl & \\[0pt]
ONNX & ONNX.jl & \\[0pt]
\end{tabular}
\end{center}
\end{frame}
\begin{frame}[label={sec:org03d6547}]{Other practical issues}
\setbeamercovered{transparent}
\begin{description}[<+->]
\item[{\alert{Julia Startup time}}] time for first epoch: \textasciitilde{}8sec since Julia 1.9
\item[{\alert{Packaging / CUDA troubles?}}] Almost never (!)
\item[{\alert{Optimized execution speed}}] \(\sim 2/3\) of pytorch
\begin{itemize}
\item Kernels generally not hand-rolled
\end{itemize}
\item[{\alert{Dataset availablility}}] many available, but not all.
\begin{itemize}
\item Generally easy to implement.
\end{itemize}
\item[{\alert{General polish}}] Sometimes memory leaks, wrong gradients, \dots{}
\item[{\alert{Sharing with other academics}}] remains difficult\(\dots\)
\end{description}
\end{frame}
\begin{frame}[label={sec:orgf645338}]{Should \alert{you} be using Julia for DL?}
\begin{columns}
\begin{column}{0.05\columnwidth}
\end{column}
\begin{column}{0.9\columnwidth}
\begin{itemize}
\item In the mainstream topics, pytorch remains unbeaten.
{\fontspec{Noto Color Emoji}[Renderer=HarfBuzz] 👸}
\item However, once you leave the beaten path, Julia/Flux shows it's colors.
{\fontspec{Noto Color Emoji}[Renderer=HarfBuzz] 💪}
\item Lesson for myself: Solve \uline{one} hard problem at a time \(\rightarrow\) theory \alert{or} implementation.
\end{itemize}
\end{column}
\begin{column}{0.05\columnwidth}
\end{column}
\end{columns}
\begin{block}{\phantom{new section}}
\pause
\begin{center}
Thanks!
\end{center}
\end{block}
\end{frame}
\end{document}